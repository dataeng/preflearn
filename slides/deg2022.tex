\documentclass[xcolor={table,usenames,dvipsnames,svgnames}, compress, aspectratio=169, 11pt]{beamer}

\usepackage{booktabs}
\usepackage{multirow}
\usepackage{dcolumn}
\usepackage{colortbl}
\usepackage[bookmarks=true]{hyperref}
\usepackage{bookmark}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{wrapfig}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\usepackage{pifont}
\usepackage[style=authoryear-icomp,backend=bibtex, mincitenames=2, maxcitenames=2]{biblatex}
\usepackage{marvosym}
\usepackage{mathtools}
\usepackage{array}
\usepackage[export]{adjustbox}
\usepackage{bm}
%\usepackage{dsfont}
\usepackage{subcaption}
\usepackage[font=scriptsize]{caption}
\usepackage{listings}
\usepackage{lstautogobble}

\definecolor{light-gray}{gray}{0.92} 

\lstset{
  autogobble=true,
  backgroundcolor=\color{light-gray},
  framexleftmargin = 0.5em,
  framexrightmargin = 0.5em,
  escapeinside=||
}

\usetheme{spinaceto}
\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}

\setbeamercovered{transparent}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\setbeamersize{text margin left=8mm}

\hypersetup{
  pdftitle={Preference Representation and Learning},
  pdfauthor={Antonis Troumpoukis, Stasinos Konstantopoulos, Denia Kanellopoulou},
  pdfkeywords={Preference Representation, Computational Logics, Gigacampus data},
  pdfsubject={In this talk we will present one of the datasets
    extracted from the Gigacampus sensors and how we have exploited it
    to advance our research on preference representation. The dataset
    is collected from sensors installed in each of the parking slots
    of the Lefkippos car park to give a timestamped occupied/available
    log. From this log we have extracted a dataset of parking acts,
    where each act records the selected slot, the slots available at
    the time, and the weather conditions. We have started using this data
    to drive and validate work on logical representations of the
    premises and contexts under which one item is preferred over
    another.}
}

\setbeamertemplate{bibliography item}{\hspace{10pt}\raise
  .2ex\hbox{\textcolor{lacamlilac}{$\boldsymbol{\oplus}$}}}

\setbeamertemplate{footline}{%
  \includegraphics[height=1cm]{images/logo-deg}
}

\AtBeginSection[]
{
  {
  \setbeamertemplate{headline}{}
  \setbeamercolor{palette primary} {fg=black, bg=lacamdarklilac}
  \setbeamercolor{section page} {fg=white, bg=lacamdarklilac}
  \begin{frame}
    \sectionpage
  \end{frame}
  }
}

\DeclareMathOperator{\mnot}{not}
\newcommand\defeq{\mathrel{\overset{\makebox[0pt]{\mbox{\normalfont\tiny\sffamily def}}}{=}}}

% \includeonly{holp-pref}

\begin{document}

\title{Preference Representation and Learning}
\author{
  \authorbox[120pt]{Antonis Troumpoukis}{\emph{Data Engineering Group}}{\dag}
  \authorbox[120pt]{Stasinos Konstantopoulos}{\emph{Data Engineering Group}}{\dag}
  \authorbox[120pt]{Denia Kanellopoulou}{\emph{Ahedd}}{\dag}\\[5pt]
}

\makeatletter
{
  \setbeamertemplate{headline}{}
  \setbeamertemplate{footline}{}
  \begin{frame}
    \titlepage
  \end{frame}
}


\begin{frame}\frametitle{Overview}

  \begin{itemize}
  \item Preference Representation
  \item Formalisms
  \item Use Cases
  \item Lefkippos Parking Dataset
  \item WIP
  \end{itemize}
  
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
  \frametitle{Preferences}

  \begin{alertblock}{}
    \begin{itemize}
      \item are ubiquitous in real life, are being studied in many sciences
      \item as {\bf soft constraints}
        \begin{itemize}
          \item too many hard constraints $\leadsto$ empty result set
          \item too few hard constraints $\leadsto$ ``needle in a haystack''
        \end{itemize}
      \item are {\bf comparative} in nature
        \begin{itemize}
          \item in many cases, a ``preference function'' cannot be easily defined
          \item Users rarely want to express their preference using numbers
        \end{itemize}
    \end{itemize}
  \end{alertblock}

  \begin{exampleblock}{Examples}
    \begin{itemize}
      \item ``I {\bf want} to fly from Athens to Rome and I {\bf prefer}
            to fly with Realiable Airlines''
      \item ``For my tomorrow flight, I would {\bf prefer} an aisle seat to a
            window seat''
    \end{itemize}
  \end{exampleblock}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
  \frametitle{Preference Representation in Databases}
  \begin{alertblock}{Preference Formulas in Relational Queries (Chomicki, 2003)}
    \begin{itemize}
      \item Preference relations between database tuples are defined using
            { \bf preference formulas}.
      \item The most preferred tuples are retrieved using winnow, a { \bf new
            preference operator}.
    \end{itemize}
  \end{alertblock}
\end{frame}


\begin{frame}\frametitle{Preference Representation in Databases (cont.)}
  
  {
    \onslide<2,5>
    Example: Given two movies of the same genre, prefer the one with the
    highest rating.
    \[
      (\mathsf{id}, \mathsf{genre}, \mathsf{rating})
        \succ
      (\mathsf{id}', \mathsf{genre}', \mathsf{rating}') 
        \equiv
          (\mathsf{genre} = \mathsf{genre}')
            \land
          (\mathsf{rating}> \mathsf{rating}')
    \]
  }%
  {
    \onslide<3,5>
    Prioritized composition: ``prefer w.r.t. $\succ_1$ and if it is
    \emph{inapplicable}, then prefer w.r.t. $\succ_2$''
    \[
      t \succ t'
        \equiv
        \big(t \succ_1 t'\big)
        \lor
        \big(\neg(t  \succ_1 t') \land
             \neg(t' \succ_1 t ) \land (t \succ_2 t')\big)
    \]
  }%
  {
    \onslide<4,5>
    Winnow operator: Return the non-dominated (i.e., best) tuples according
    to a preference relation:
    \[
      w_\succ(R) = \{ 
        t \in R :
        \neg \exists t' \in R\ \ \text{such that}\ \ t' \succ t
      \}
    \]
  }
\end{frame}


\begin{frame}
  \frametitle{Preference Representation using Higher-Order Logic Programming}

  \begin{alertblock}{Extensions of Logic Programming for Preference
    Representation (Troumpoukis, 2019)}
  	\begin{itemize}
      \item Preferences are binary {\bf relations}
      \item Operators over preferences are operators that take 
            {\bf relations as parameters}.
      \item Preference relations and preference operators can be expressed
            in the {\bf same language} using Higher-Order Logic Programming.
    \end{itemize}
  \end{alertblock}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Preference Representation using Higher-Order Logic Programming
      (cont.)}
  
  {
    \onslide<2,5>
    Example: Given two movies of the same genre, prefer the one with the
    highest rating.
    \begin{lstlisting}{prolog}
      my_pref(X,Y) :- genre(X,D), genre(Y,D), rating(X,N), rating(Y,M), N > M.
    \end{lstlisting}
  }%
  {
    \onslide<3,5>
    Prioritized composition: ``prefer w.r.t. \lstinline{C1} and if it is
    \emph{inapplicable}, then prefer w.r.t. \lstinline{C2}''
    \begin{lstlisting}{prolog}
      prioritized(C1,C2)(X,Y) :- C1(X,Y).
      prioritized(C1,C2)(X,Y) :- \+ C1(X,Y), \+ C1(Y,X), C2(X,Y).
    \end{lstlisting}
  }%
  {
    \onslide<4,5>
    Winnow operator: Return the non-dominated (i.e., best) tuples according
    to a preference relation:
    \begin{lstlisting}{prolog}
      winnow(C,R)(X) :- R(X), \+ bypassed(C,R,X).
      bypassed(C,R,X) :- R(Z), C(Z,X).
    \end{lstlisting}
  }
\end{frame}


\begin{frame}[fragile]
  \frametitle{Preference Representation in the Semantic Web}

  \begin{alertblock}{The SPREFQL language
    (Troumpoukis et al., 2017)}
  	\begin{itemize}
      \item extension of SPARQL with a solution modifier to select the most
            preferred solutions
      \item SPREFQL queries can be rewritten into SPARQL 1.1
    \item syntax allows to recognize opportunities to apply
          specialized algorithms
    \end{itemize}
  \end{alertblock}
  \begin{lstlisting}
    SELECT ?film ?genre ?runtime WHERE {
      ?film a :film. 
      ?film :genre ?genre.
      ?film :runtime ?runtime.
    }
    PREFER (?film1 ?genre1 ?runtime1)
    TO     (?film2 ?genre2 ?runtime2)
    IF (?genre1 = ?genre2 && ?runtime1 > ?runtime2)
  \end{lstlisting}
\end{frame}


\begin{frame}\frametitle{What is a Good Preference Representation Formalism?}

  \begin{itemize}
  \item Expressive
  \item Succinct and human-editable
%     \begin{itemize}
%     \item Really editable, not as in XML
%     \end{itemize}
  \item Amenable to efficient processing
%     \begin{itemize}
%     \item If there are alternative ways to get equivalent results,
%       the most efficient way should have the most straightforward syntax
%     \end{itemize}
  \item Learnable
  \end{itemize}
  
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}\frametitle{Preference Use Cases?}
%\frametitle{The Seaside Hotel Room Problem}

  \begin{itemize}
  \item Lack of use cases and datasets where preferences are
    more complex than looking for
    \begin{itemize}
      \item a cheap hotel that is close to the seaside;
      \item or multi-faceted book/movie recommendations
    \end{itemize}
  \item Databases literature: emphasises efficient retrieval rather
    than the syntax and semantics of complex preference expressions
  \item ???
  \end{itemize}

%   \begin{block}{%
%       These supported a great deal of work on designing a preferences
%       formalism that is amenable to optimizations, but cannot support
%       work on the syntax and semantics of complex preference
%       expressions.}
%     We saw an opportunity while torturing the Gigacampus data.
%   \end{block}
  
\end{frame}

{
\usebackgroundtemplate{%
  \includegraphics[width=\paperwidth]{images/giga-campus-trees.jpg}}
\begin{frame}[plain]
\end{frame}
}

\begin{frame}\frametitle{Giga Campus}
  \begin{itemize}
    \item Collaboration with Vodafone for:
    \begin{itemize}
      \item Private {\bf 5G network} deployment in campus
      \item {\bf IoT sensors} \& connectivity solutions for Facility management,
            Security \& Fleet monitoring solutions in Lefkippos building and
            across campus 
    \end{itemize}
    \item Collaboration with Fuelics for:
      \begin{itemize}
        \item {\bf Water management} solutions 
      \end{itemize}
    \item Potential {\bf synergies} explored with other SMEs/mid-caps
          (providers, integrators, users) {\bf to enrich or use} the test-bed
          facilities
    \item Infrastructure \& Data are made available for {\bf R\&D exploitation}
  \end{itemize}
\end{frame}


{
\usebackgroundtemplate{%
  \includegraphics[width=\paperwidth]{images/giga-campus-dashboards.jpg}}
\begin{frame}[plain]
\end{frame}
}

\begin{frame}\frametitle{Lefkippos Parking Data}

  \begin{itemize}
  \item A log of timestamped occupied/available status for each of the 30
    parking slots of the Lefkippos car park
    \begin{itemize}
    \item 288844 log lines
    \item containing 2845 instances of free-to-occupied transitions
    \item during the period 1 Jan - 30 Apr
    \end{itemize}
  \item 53968 preferance pairs of a slot having been preferenced
    over each one of the other slots available at that time
  \item Tried to update the above statistcs with new data since May
    \begin{itemize}
    \item Sensors stopped reacting some time in early May
    \item Log lines are being produced, but stuck to the same value
    \end{itemize}
  \end{itemize}
  
\end{frame}



\begin{frame}\frametitle{Lefkippos Parking Data: Nicely inconsistent}

  \begin{itemize} 
  \item The dataset is inconsistent, but not a coin
  \item Different decisions are made over the exact same configuration 
  \item Are not uniformly distributed
  \item There are favourites, but do not always win
  \end{itemize}
  
\end{frame}



\begin{frame}\frametitle{Lefkippos Parking Data: Nicely inconsistent}

\begin{center}
\includegraphics[scale=1]{images/park-1}

\vskip 3 em

\begin{tabular}{ccccccccc}
   2 &  4 &  2 & 22 &  5 &  3 &  1 &  1 &  1 \\
\hline
  56 & 58 & 59 & 71 & 73 & 74 & 75 & 78 & 83 \\
\end{tabular}
\end{center}

\end{frame}



\begin{frame}\frametitle{Lefkippos Parking Data: Nicely inconsistent}

\begin{center}
\includegraphics[scale=1]{images/park-2}

\vskip 3 em

\begin{tabular}{ccccccc}
   1 &  2 &  3 &  1 &  1 &  1 &  3 \\
\hline
  55 & 56 & 58 & 59 & 71 & 72 & 83 \\
\end{tabular}
\end{center}

\end{frame}



\begin{frame}\frametitle{Lefkippos Parking Data: Nicely inconsistent}

\begin{center}
\includegraphics[scale=1]{images/park-3}

\vskip 3 em

\begin{tabular}{cccccc}
   2 &  3 &  1 &  1 &  4 &  1 \\
\hline
  56 & 58 & 67 & 72 & 73 & 75 \\
\end{tabular}
\end{center}

\end{frame}



\begin{frame}\frametitle{Lefkippos Parking Data: Nicely inconsistent}

\begin{center}
\includegraphics[scale=1]{images/park-4}

\vskip 3 em

\begin{tabular}{cccccc}
   1 &  1 &  1 &  1 &  5 &  2 \\
\hline
  54 & 71 & 72 & 75 & 78 & 85 \\
\end{tabular}
\end{center}

\end{frame}



\begin{frame}\frametitle{Lefkippos Parking Data: Nicely inconsistent}

\begin{center}
\includegraphics[scale=1]{images/park-5}

\vskip 3 em

\begin{tabular}{cccccccc}
   1 &  1 &  1 &  2 &  1 &  3 &  7 &  1 \\
\hline
  54 & 55 & 56 & 58 & 71 & 73 & 74 & 75 \\
\end{tabular}
\end{center}

\end{frame}



\begin{frame}\frametitle{Lefkippos Parking Data: Nicely inconsistent}

\begin{center}
\includegraphics[scale=1]{images/park-6}

\vskip 3 em

\begin{tabular}{ccccc}
   1 &  6 &  1 &  2 &  1 \\
\hline
  69 & 71 & 74 & 77 & 84 \\ 
\end{tabular}
\end{center}

\end{frame}



\begin{frame}\frametitle{Lefkippos Parking Data: Nicely inconsistent}

\begin{center}
\includegraphics[scale=1]{images/park-7}

\vskip 3 em

\begin{tabular}{ccccc}
   1 &  1 &  1 &  1 & 12 \\
\hline
  56 & 59 & 74 & 75 & 84 \\ 
\end{tabular}
\end{center}

\end{frame}



\begin{frame}\frametitle{Lefkippos Parking Data: Nicely inconsistent}

\begin{center}
\includegraphics[scale=1]{images/park-8}

\vskip 3 em

\begin{tabular}{ccccc}
   1 &  6 &  1 &  2 &  1 \\
\hline
  54 & 56 & 58 & 62 & 72 \\ 
\end{tabular}
\end{center}

\end{frame}



\begin{frame}\frametitle{Lefkippos \\ Parking \\ Data: \\ Nicely \\ inconsistent}

\begin{flushright}
\vskip -4cm
\includegraphics[scale=0.27]{images/parking_preference}
\end{flushright}
  
\end{frame}


\begin{frame}\frametitle{WIP: Manual rules}

  \begin{itemize}
  \item Encode some base preference relations (using common sense):
        e.g., given two slots:
    \begin{itemize}
      \item prefer the one {\color{orange} closest to the entrance}
            (\lstinline{pref_distance})
      \item prefer the one that {\color{violet} has empty slots available on
            either side} (\lstinline{pref_sparse})
      \item prefer the one {\color{olive} first seen while entering the parking} 
            (\lstinline{pref_precedes})
    \end{itemize}
  \item Construct combinations using previously defined preference relations and
        preference compositions (e.g., \lstinline{prioritized}) and calculate
        positive and negative preference coverage:
  \end{itemize}
  \vspace{-1em}
  \begin{center}
  {\small
  \begin{tabular}{|l|c|c|}
    \hline
    {\bf Preference} & {\bf POS cover} & {\bf NEG cover} \\
    \hline
    \lstinline{pref_distance}                    & 0.45 & 0.50 \\
    \lstinline{pref_sparse}                      & 0.19 & 0.22 \\
    \lstinline{pref_precedes}                    & 0.36 & 0.38 \\
    \hline
    \lstinline{prioritized(pref_distance,...)}   & 0.46 & 0.52 \\
    \lstinline{prioritized(pref_sparse,...)}     & 0.43 & 0.46 \\
    \lstinline{prioritized(pref_precedes,...)}   & 0.45 & 0.45 \\
    \hline
  \end{tabular}
  }
  \end{center}
\end{frame}


\begin{frame}\frametitle{WIP: Context}

  \begin{itemize}
  \item It seems (from the data and from interviews) that {\bf context}
    changes the preference: e.g.,
    \begin{itemize}
    \item I prefer a slot with {\color{violet} empty slots on either side},
      but if it's {\bf raining} I'll take the slot {\color{orange} closest
      to the entrance} no matter how cramped it is
    \item There are {\bf times during the day} where I know it's unlikely to
      find a preferred slot, so no point in going around; I'll park on
      the {\color{olive} first available slot}. (Of course it might be that a
      preferred slot is available after all, but too late now.)
    \end{itemize}
  \item These can be expressed with the constructs shown above, but a
    different syntax would give more straightforward and semantically
    transparent rules
    \begin{itemize}
      \item Write some combinator rules which combine preferences and context?
      \item Use some alternative formalism (if exists)?
      \item Work out a syntax and a semantics?
%     \item Are rules ordered? if not, what does it mean when multiple
%       contexts apply?
%     \item If yes, how can I scope this to organize different
%       preferences into an overall KB/logic program?
    \end{itemize}
  \end{itemize}
  
\end{frame}


\begin{frame}[fragile]\frametitle{WIP: ILP}

  \begin{itemize}
  \item Encode base preference relations, preference compositions, and
    context predicates as background knowledge (and transform it in Prolog)
  \item WIP: Use Aleph to learn a theory (based on positive, negative preference
    examples)
  \end{itemize}
  {\tiny
  \begin{lstlisting}
    [theory]

    [Rule 2] [Pos cover = 15212 Neg cover = 13225]
    target(A,B,C) :-
      cons_pref(distance,D), apply(D,B,A,C).

    Accuracy = 0.5182621962427852
    [Training set summary] [[15212,13225,39190,41177]]
    [time taken] [24063.435770162]
    [total clauses constructed] [235588]
  \end{lstlisting}
  }
\end{frame}


\begin{frame}\frametitle{Thank you for your attention}

  \begin{itemize}
  \item Publishing the data
    \begin{itemize}
    \item We don't foresee any major problems,
      but it's not formally finalized 
    \end{itemize}
  \item But you are welcome to it, just ask
  \item Our Plan B: The Moral Machine
  \end{itemize}

\vskip 2em

\begin{alertblock}{}\centering\alert{Any questions?}\end{alertblock}

\end{frame}




% \begin{frame}[allowframebreaks]
%   \frametitle{References}
%   \printbibliography
% \end{frame}

\end{document}
