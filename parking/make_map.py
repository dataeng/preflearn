import sys
import json

print( "WKT,name,description" )

doc = json.load( open("static_data.json","r") )

for asset_id in doc["parking"]["constants"].keys():
    lat = doc["parking"]["constants"][asset_id]["pointDto"]["latitude"]
    lon = doc["parking"]["constants"][asset_id]["pointDto"]["longitude"]
    desc = doc["parking"]["constants"][asset_id]["assetName"]
    print ( '"POINT (' + str(lon) + ' ' + str(lat) + ')",asset_'+asset_id+','+desc )



