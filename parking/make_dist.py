import sys
import json
import haversine as hs

doc = json.load( open("static_data.json","r") )

entrance = (23.8146217,37.9963371)

for asset_id in doc["parking"]["constants"].keys():
    lat = doc["parking"]["constants"][asset_id]["pointDto"]["latitude"]
    lon = doc["parking"]["constants"][asset_id]["pointDto"]["longitude"]
    p = (lon,lat)
    print( "distance( entrance, asset_" + asset_id + ", " + str(int(1000*hs.haversine(p, entrance))) + " )." )

