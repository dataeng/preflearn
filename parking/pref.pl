:- [bg].

preference_types( distance ).
preference_types( precedes ).
preference_types( sparse ).

winnow( PrefType, X, Time ) :-
    asset(X),
    \+ apply( PrefType, _, X, Time ).

apply( distance, X, Y, _Time ) :-
    distance( entrance, X, DX ),
    distance( entrance, Y, DY ),
    D is DY - DX,
    D > 5.

apply( precedes, X, Y, _Time ) :-
    largely_precedes( X, Y ).

apply( sparse, X, Y, Time ) :-
    immediatelly_precedes(A, X), available(A,Time),
    immediatelly_precedes(X, B), available(B,Time),
    immediatelly_precedes(C, Y), \+ available(C,Time).
apply( sparse, X, Y, Time ) :-
    immediatelly_precedes(A, X), available(A,Time),
    immediatelly_precedes(X, B), available(B,Time),
    immediatelly_precedes(Y, C), \+ available(C,Time).
