:- [data].

temperature(_V,T) :- weather(null,_,_,_,_,_,_,T),!,fail.
temperature( V,T) :- weather(V,   _,_,_,_,_,_,T).

humidity(_V,T) :- weather(_,null,_,_,_,_,_,T),!,fail.
humidity( V,T) :- weather(_,V,   _,_,_,_,_,T).

wind_speed(_V,T) :- weather(_,_,null,_,_,_,_,T),!,fail.
wind_speed( V,T) :- weather(_,_,V   ,_,_,_,_,T).

wind_deg(_V,T) :- weather(_,_,_,null,_,_,_,T),!,fail.
wind_deg( V,T) :- weather(_,_,_,V   ,_,_,_,T).

wind_gust(_V,T) :- weather(_,_,_,_,null,_,_,T),!,fail.
wind_gust( V,T) :- weather(_,_,_,_,V,   _,_,T).

pressure(_V,T) :- weather(_,_,_,_,_,null,_,T),!,fail.
pressure( V,T) :- weather(_,_,_,_,_,V,   _,T).

feels_like(_V,T) :- weather(_,_,_,_,_,_,null,T),!,fail.
feels_like( V,T) :- weather(_,_,_,_,_,_,V,   T).



asset( asset_33854 ).
asset( asset_33855 ).
asset( asset_33856 ).
asset( asset_33857 ).
asset( asset_33858 ).
asset( asset_33859 ).
asset( asset_33860 ).
asset( asset_33861 ).
asset( asset_33862 ).
asset( asset_33863 ).
asset( asset_33864 ).
asset( asset_33865 ).
asset( asset_33866 ).
asset( asset_33867 ).
asset( asset_33868 ).
asset( asset_33869 ).
asset( asset_33870 ).
asset( asset_33871 ).
asset( asset_33872 ).
asset( asset_33873 ).
asset( asset_33874 ).
asset( asset_33875 ).
asset( asset_33876 ).
asset( asset_33877 ).
asset( asset_33878 ).
asset( asset_33879 ).
asset( asset_33880 ).
asset( asset_33881 ).
asset( asset_33882 ).
asset( asset_33883 ).
asset( asset_33884 ).
asset( asset_33885 ).



immediatelly_precedes( asset_33854, asset_33855 ).
immediatelly_precedes( asset_33855, asset_33856 ).
immediatelly_precedes( asset_33856, asset_33857 ).
immediatelly_precedes( asset_33857, asset_33858 ).
immediatelly_precedes( asset_33858, asset_33859 ).
immediatelly_precedes( asset_33859, asset_33860 ).
immediatelly_precedes( asset_33860, asset_33861 ).
immediatelly_precedes( asset_33861, asset_33862 ).
immediatelly_precedes( asset_33862, asset_33863 ).
immediatelly_precedes( asset_33863, asset_33864 ).
immediatelly_precedes( asset_33864, asset_33865 ).
immediatelly_precedes( asset_33865, asset_33866 ).
immediatelly_precedes( asset_33866, asset_33867 ).
immediatelly_precedes( asset_33867, asset_33868 ).
immediatelly_precedes( asset_33868, asset_33869 ).

immediatelly_precedes( asset_33869, asset_33885 ).
immediatelly_precedes( asset_33885, asset_33884 ).
immediatelly_precedes( asset_33884, asset_33883 ).
immediatelly_precedes( asset_33883, asset_33882 ).
immediatelly_precedes( asset_33882, asset_33881 ).
immediatelly_precedes( asset_33881, asset_33880 ).
immediatelly_precedes( asset_33880, asset_33879 ).
immediatelly_precedes( asset_33879, asset_33878 ).
immediatelly_precedes( asset_33878, asset_33877 ) .
immediatelly_precedes( asset_33877, asset_33876 ).
immediatelly_precedes( asset_33876, asset_33875 ).
immediatelly_precedes( asset_33875, asset_33874 ).
immediatelly_precedes( asset_33874, asset_33873 ).
immediatelly_precedes( asset_33873, asset_33872 ).
immediatelly_precedes( asset_33872, asset_33871 ).

precedes( X, Y ) :-
    immediatelly_precedes( X, Y ).
precedes( X, Y ) :-
    immediatelly_precedes( X, Z ),
    precedes( Z, Y ).


closely_precedes( X, Y ) :-
    immediatelly_precedes( X, Y ).
closely_precedes( X, Y ) :-
    immediatelly_precedes( X, A ),
    immediatelly_precedes( A, Y ).
closely_precedes( X, Y ) :-
    immediatelly_precedes( X, A ),
    immediatelly_precedes( A, B ),
    immediatelly_precedes( B, Y ).
closely_precedes( X, Y ) :-
    immediatelly_precedes( X, A ),
    immediatelly_precedes( A, B ),
    immediatelly_precedes( B, C ),
    immediatelly_precedes( C, Y ).

largely_precedes( X, Y ) :-
    precedes( X, Y ),
    \+ closely_precedes( X, Y ).



distance( entrance, asset_33854, 30 ).
distance( entrance, asset_33855, 27 ).
distance( entrance, asset_33856, 26 ).

distance( entrance, asset_33857, 23 ).
distance( entrance, asset_33858, 21 ).
distance( entrance, asset_33859, 20 ).
distance( entrance, asset_33860, 21 ).
distance( entrance, asset_33861, 19 ).
distance( entrance, asset_33862, 20 ).
distance( entrance, asset_33863, 20 ).
distance( entrance, asset_33864, 22 ).
distance( entrance, asset_33865, 24 ).

distance( entrance, asset_33866, 26 ).
distance( entrance, asset_33867, 26 ).
distance( entrance, asset_33868, 28 ).
distance( entrance, asset_33869, 33 ).

distance( entrance, asset_33870, 33 ).
distance( entrance, asset_33871, 28 ).
distance( entrance, asset_33872, 26 ).

distance( entrance, asset_33873, 22 ).
distance( entrance, asset_33874, 19 ).
distance( entrance, asset_33875, 16 ).

distance( entrance, asset_33876, 15 ).
distance( entrance, asset_33877, 14 ).
distance( entrance, asset_33878, 14 ).
distance( entrance, asset_33879, 14 ).
distance( entrance, asset_33880, 15 ).
distance( entrance, asset_33881, 14 ).

distance( entrance, asset_33882, 16 ).
distance( entrance, asset_33883, 18 ).
distance( entrance, asset_33884, 21 ).

distance( entrance, asset_33885, 27 ).
