import influxdb_client
import pandas as pd

from bokeh.io import output_file, show
from bokeh.models import BasicTicker, ColorBar, LinearColorMapper, ColumnDataSource, PrintfTickFormatter
from bokeh.plotting import figure
from bokeh.transform import transform

ORG='xxx'
TOK='xxx'
HOST='xxx'

client = influxdb_client.InfluxDBClient( url=HOST, token=TOK, org=ORG, debug=False )

q='from(bucket: "gigacampus") \
       |> range(start: 2022-01-01T00:00:00Z, stop: 2022-05-01T00:00:00Z) \
       |> filter(fn: (r) => r._measurement == "parking" and r._field == "occupied" ) \
       |> keep( columns: [ "asset_id", "_value", "_time"] ) \
       |> sort( columns: ["_time"], desc: false )'
tables = client.query_api().query( org=ORG, query=q )

for t in tables:
    prev_value = 0.0
    for r in t:
        value = r["_value"]

        if (value == 1.0) and (prev_value == 0.0):

            asset_id = r["asset_id"]
            parking_time = r["_time"].isoformat('T')
            print( "parking_act( asset_" + asset_id + " , '" + parking_time + "' )." )

            q2 = 'from(bucket: "gigacampus") \
                         |> range(start: 2021-01-01T00:00:00Z, stop: '+parking_time+') \
                         |> filter(fn: (r) => r._measurement == "parking" and r._field == "occupied") \
                         |> sort(columns: ["_time"], desc: false) \
                         |> last(column: "_time") \
                         |> keep(columns: ["asset_id", "_time", "_value"]) '
            #print(q2)
            tables2 = client.query_api().query( org=ORG, query=q2 )
            for t2 in tables2:
                for r2 in t2:
                    # The time range is exclusive of the stop time, so this might also
                    # fetch the slot of the current parking act if it was not occupied before.
                    # So the current asset must be excluded explicitly.
                    if (not asset_id == r2["asset_id"]) and (r2["_value"] < 0.5): 
                        print( "available( asset_" + r2["asset_id"] + ", '" + parking_time + "' )." )

            q3 = 'from(bucket: "gigacampus") \
                         |> range(start: 2021-01-01T00:00:00Z, stop: '+parking_time+') \
                         |> filter(fn: (r) => r._measurement == "weather") \
                         |> sort(columns: ["_time"], desc: false) \
                         |> last(column: "_time") \
                         |> keep(columns: ["_field", "_value"]) '
            #print( "DEBUG: " + q3 )
            tables3 = client.query_api().query( org=ORG, query=q3 )
            temperature = "null"
            feels_like = "null"
            humidity = "null"
            wind_speed = "null"
            wind_deg = "null"
            wind_gust = "null"
            pressure = "null"
            for t3 in tables3:
                # TODO: there are gaps in the weather record, so the distance between
                # parking_time and weather time should be checked to ensure that the
                # most recent weather datapoint is recent enough.
                for r3 in t3:
                    if r3["_field"] == "temp":
                        if temperature == "null": temperature = r3["_value"]
                        else: print("ERROR")
                    elif r3["_field"] == "humidity":
                        if humidity == "null": humidity = r3["_value"]
                        else: print("ERROR")
                    elif r3["_field"] == "wind_speed":
                        if wind_speed == "null": wind_speed = r3["_value"]
                        else: print("ERROR")
                    elif r3["_field"] == "wind_deg":
                        if wind_deg == "null": wind_deg = r3["_value"]
                        else: print("ERROR")
                    elif r3["_field"] == "wind_gust":
                        if wind_gust == "null": wind_gust = r3["_value"]
                        else: print("ERROR")
                    elif r3["_field"] == "pressure":
                        if pressure == "null": pressure = r3["_value"]
                        else: print("ERROR")
                    elif r3["_field"] == "feels_like":
                        if feels_like == "null": feels_like = r3["_value"]
                        else: print("ERROR")
                    elif r3["_field"] == "temp_max": pass
                    elif r3["_field"] == "temp_min": pass
                    else:
                        print( "ERROR: unknown field " + r3["_field"] )
            print( "weather( " + str(temperature) + ", " + str(humidity) + ", " +
                   str(wind_speed) + ", " + str(wind_deg) + ", " + str(wind_gust) + ", " +
                   str(pressure) + ", " + str(feels_like) + ", " +
                   "'" + parking_time + "' )." )

        prev_value = value
